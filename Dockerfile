FROM ubuntu:16.04

RUN apt-get -qq -y update && \
    apt-get -qq -y install openjdk-8-jdk maven curl wget unzip vim && \
    rm -rf /var/cache/apt /var/lib/apt/lists/*

RUN wget https://services.gradle.org/distributions/gradle-5.2.1-bin.zip -P /tmp
RUN unzip -d /opt/gradle /tmp/gradle-*.zip
#RUN vi /etc/profile.d/gradle.sh
RUN export GRADLE_HOME=/opt/gradle/gradle-5.2.1
RUN export PATH=${GRADLE_HOME}/bin:${PATH}

WORKDIR /src
ADD . .
RUN ./service.sh dev_build
CMD ["/src/service.sh", "dev_run", "data/example"]
