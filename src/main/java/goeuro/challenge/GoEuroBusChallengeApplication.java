package goeuro.challenge;

import goeuro.challenge.data.DataGenerator;
import goeuro.challenge.data.DataLoader;
import goeuro.challenge.data.FileDataLoader;
import goeuro.challenge.service.DirectRouteService;
import goeuro.challenge.service.GraphBasedDirectRouteServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class GoEuroBusChallengeApplication {

	static Logger logger = LoggerFactory.getLogger(GoEuroBusChallengeApplication.class.getName());

	public static void main(String[] args) {
		if (args != null) {
      for (String arg : args) {
				logger.info(" >>> {} <<<", arg);
			}
		}
		SpringApplication.run(GoEuroBusChallengeApplication.class, args);
	}

	@Bean
	@Autowired
	DataLoader dataLoader(ApplicationArguments applicationArguments) {
		List<String> args = applicationArguments.getNonOptionArgs();
		if (args.isEmpty()) {
			throw new RuntimeException("No data file provided");
		}
    logger.info("------- {}", args.toString());
    return new FileDataLoader(args.get(0));
	};

  @Bean
  DirectRouteService directRouteService(DataLoader dataLoader) {
    return new GraphBasedDirectRouteServiceImpl(dataLoader);
	}

//	@Bean
	ApplicationRunner applicationRunner() {
  	return args -> {
  		logger.warn("Args {}", args);
  		new DataGenerator("/Users/vrudyk/workspace/goeuro-bus-challenge/data/example123").generate();
		};
	}
}
