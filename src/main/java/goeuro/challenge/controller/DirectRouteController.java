package goeuro.challenge.controller;

import goeuro.challenge.response.DirectRouteResponse;
import goeuro.challenge.service.DirectRouteService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RequestMapping("/api")
@RestController
public class DirectRouteController {

  DirectRouteService directRouteService;

  public DirectRouteController(DirectRouteService directRouteService) {
    this.directRouteService = directRouteService;
  }

  @GetMapping("/direct")
  public Object directRoute(@RequestParam(value = "dep_sid") final Integer departure,
                            @RequestParam(value = "arr_sid") final Integer arrival) {

    boolean hasDirectRoute = this.directRouteService.containsDirectRoute(departure, arrival);
    List<Integer> list = new ArrayList<>(1_000_000);
    for (int i = 0; i < 1_000_000; i++) {
      list.add(i);
    }
    return new DirectRouteResponse(departure, arrival, hasDirectRoute);
  }

//  private Stream<Route> getRoutes() {
//    int ROUTES_COUNT = 20;
//
//    List<Route> routes = new ArrayList<>();
//
//    for (int i = 0; i < ROUTES_COUNT; i++) {
//      Route route = new Route();
//
//      route.routeNumber = i;
//      Set<Integer> integers = new HashSet<>();
//      Random random = new Random();
//
//      for (int i1 = 0; i1 < 50; i1++) {
//        Integer next = random.nextInt(10000);
//        integers.add(next);
//      }
//
//      route.stations.addAll(integers);
//      routes.add(route);
//    }
//
//    return routes.stream();
//  }


}
