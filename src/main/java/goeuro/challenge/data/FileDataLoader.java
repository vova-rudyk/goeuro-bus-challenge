package goeuro.challenge.data;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileDataLoader implements DataLoader {

  private Logger logger = LoggerFactory.getLogger(FileDataLoader.class.getName());

  private String path;

  public FileDataLoader(String path) {
    this.path = path;
    logger.info("Create Data loader with path {}", path);
  }

  public Route[] load() {
    Path dataSourcePath = Paths.get(this.path);
    Stream<String> lines = null;
    try {
      lines = Files.lines(dataSourcePath);
    } catch (IOException e) {
      e.printStackTrace();
      System.exit(1);
    }

    List<Route> routes = lines.skip(1).parallel().map(Route::new).collect(Collectors.toList());

    logger.info("Routes number {}", routes.size());

    return routes.toArray(new Route[0]);
  }
}
