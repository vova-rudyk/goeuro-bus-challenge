package goeuro.challenge.data;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.util.Assert;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Data
@EqualsAndHashCode(of = {"routeNumber"})
public class Route {
  public Integer routeNumber;
  public List<Integer> stations;;

  public Route(String route) {
    Assert.hasLength(route, "Route is empty");

    String[] routeData = route.split(" ");
    // check routeData is empty

    this.routeNumber = Integer.valueOf(routeData[0]);
    this.stations =
        Arrays.stream(routeData)
            .skip(1)
            .mapToInt(Integer::valueOf)
            .boxed()
            .collect(Collectors.toList());
  }

  public Route(Integer routeNumber, List<Integer> stations) {
    this.routeNumber = routeNumber;
    this.stations.addAll(stations);
  }
}
