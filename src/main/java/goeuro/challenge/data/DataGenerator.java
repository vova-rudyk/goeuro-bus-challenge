package goeuro.challenge.data;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class DataGenerator {

  Logger logger = LoggerFactory.getLogger(DataGenerator.class.getName());

  public static final int STATIONS_NUMBER = 1_000;
  public static final int ROUTES_NUMBER = 5_000;
  private Path file;

  public DataGenerator(String path) {
    file = Paths.get(path);
  }

  public void generate() {
    if (!createFile(this.file)) return;

    Random random = new Random(STATIONS_NUMBER);

    StringBuilder builder = new StringBuilder();
    builder.append(ROUTES_NUMBER).append("\n");

    for (int r = 0; r < ROUTES_NUMBER; r++) {
      builder.append(r).append(" ");

      Set<Integer> data = new HashSet<>();
      for (int i = 0; i < STATIONS_NUMBER; i++) {
        Integer next = random.nextInt(Integer.MAX_VALUE);
        data.add(next);
        builder.append(next).append(" ");
      }
      builder.append("\n");
      logger.info("Route {} generated, size {}", r, data.size());
    }

    logger.info("Data generated");

    try {
      Files.write(file, builder.toString().getBytes());
    } catch (IOException e) {
      e.printStackTrace();
    }

    logger.info("Data is written to the file, {}", file.toAbsolutePath().toString());
  }

  private boolean createFile(Path file) {
    if (!Files.exists(file)) {
      try {
        Files.createFile(file);
        logger.info("File created");
        return true;
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    logger.info("File already exists");
    return false;
  }
}
