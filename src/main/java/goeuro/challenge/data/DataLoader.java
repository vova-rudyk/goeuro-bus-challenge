package goeuro.challenge.data;

public interface DataLoader {
  Route[] load();
}
