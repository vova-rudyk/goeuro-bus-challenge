package goeuro.challenge.service;

public interface DirectRouteService {
  boolean containsDirectRoute(Integer departure, Integer arrival);
}
