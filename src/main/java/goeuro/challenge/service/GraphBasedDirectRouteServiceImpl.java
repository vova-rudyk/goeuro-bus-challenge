package goeuro.challenge.service;

import goeuro.challenge.data.DataLoader;
import goeuro.challenge.data.Route;
import org.jgrapht.Graph;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.interfaces.ShortestPathAlgorithm;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class GraphBasedDirectRouteServiceImpl implements DirectRouteService {

  Logger logger = LoggerFactory.getLogger(GraphBasedDirectRouteServiceImpl.class.getName());

  protected ShortestPathAlgorithm<Integer, DefaultEdge> shortestPathAlgorithm;
  protected Graph<Integer, DefaultEdge> graph;

  public GraphBasedDirectRouteServiceImpl(DataLoader dataLoader) {
    initDataSource(dataLoader);
  }

  private void initDataSource(DataLoader dataLoader) {
    Route[] routes = dataLoader.load();
    this.logger.info("Start filling data graph");
    this.graph = new SimpleGraph<>(DefaultEdge.class);

    for (Route route : routes) {
      List<Integer> stations = route.stations;
      logger.info("Route id {}", route.routeNumber);
      logger.info("Route stations number {}", route.stations.size());

        for (int i = 1; i < stations.size(); i++) {
          this.graph.addVertex(stations.get(i));
          this.graph.addVertex(stations.get(i-1));
          this.graph.addEdge(stations.get(i), stations.get(i-1));
        }
    }

    this.shortestPathAlgorithm = new DijkstraShortestPath<>(graph);
    this.logger.info("Finished graph population {}", graph.vertexSet().size());
  }

  public boolean containsDirectRoute(Integer departure, Integer arrival) {
    GraphPath<Integer, DefaultEdge> path = this.shortestPathAlgorithm.getPath(departure, arrival);
    boolean pathExists = path != null;
    this.logger.info("Path for {} <-> {} exists {}", departure, arrival, pathExists);
    if (pathExists) {
      this.logger.info("Path for {} <-> {} has {} edges", departure, arrival, path.getEdgeList().size());
    }
    return pathExists;
  }

}
