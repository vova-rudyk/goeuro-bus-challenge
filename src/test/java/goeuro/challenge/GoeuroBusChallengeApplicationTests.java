package goeuro.challenge;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {A.class})
public class GoeuroBusChallengeApplicationTests {

	@Test
	public void contextLoads() {
	}

}
