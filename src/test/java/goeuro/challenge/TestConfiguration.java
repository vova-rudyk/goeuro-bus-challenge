package goeuro.challenge;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.DefaultApplicationArguments;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TestConfiguration {
  @Bean
  public ApplicationArguments applicationArguments() {
    return new DefaultApplicationArguments(new String[]{"data/example"});
  }
}
